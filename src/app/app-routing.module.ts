import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { SearchResultComponent } from './home/search-result/search-result.component';


const routes: Routes = [
    {
        path: 'home',
        component: HomeComponent,
        children: [{
            path: 'search-result/:from/:to/:dateFrom/:dateTo',
            component: SearchResultComponent
        }]
    },
    {
        path: '**',
        redirectTo: 'home',
        pathMatch: 'full'
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
