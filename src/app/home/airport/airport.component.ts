import { Component, ElementRef, EventEmitter, forwardRef, HostListener, Input, OnInit, Output } from '@angular/core';
import { FlightService } from '../flight.service';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';

const noop = () => {
};

export const CUSTOM_CONTROL_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => AirportComponent),
  multi: true
};


@Component({
  selector: 'app-airport',
  templateUrl: './airport.component.html',
  styleUrls: ['./airport.component.css'],
  providers: [CUSTOM_CONTROL_VALUE_ACCESSOR]
})
export class AirportComponent implements OnInit, ControlValueAccessor {
  // public query = '';
  @Input() countries: Array<Object>;
  @Output() airportChanged: EventEmitter<Object> = new EventEmitter();
  public filteredList = [];
  public elementRef: ElementRef;
  private innerValue: any = '';
  private onTouchedCallback: Function  = noop;
  private onChangeCallback: Function = noop;

  constructor(private myElement: ElementRef) {
    this.elementRef = myElement;
  }

  @HostListener('document:click', ['$event'])
  onDocumentClick(event: Event) {
    if (this.elementRef.nativeElement.contains(event.target)) {
      return;
    } else {
      this.filteredList = [];
    }
  }

  ngOnInit() {
  }

  get query(): any {
    return this.innerValue;
  }

  set query(v: any) {
    if (v !== this.innerValue) {
      this.innerValue = v;
      this.onChangeCallback(v);
    }
  }

  writeValue(value: any) {
    if (value !== this.innerValue) {
      this.innerValue = value;
    }
  }

  registerOnChange(fn: Function) {
    this.onChangeCallback = fn;
  }

  registerOnTouched(fn: Function) {
    this.onTouchedCallback = fn;
  }


  filter() {
    if (this.query !== '') {
      this.filteredList = this.countries.filter((el: any) => {
        return el.name.toLowerCase().indexOf(this.query.toLowerCase()) > -1;
      });
    } else {
      this.filteredList = [];
    }
  }

  select(item: any, obj: Object) {
    this.airportChanged.emit(obj);
    this.query = item;
    this.filteredList = [];
  }

}
