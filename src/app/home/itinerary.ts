export interface Itinerary {
  [key: string]: string[];
}
