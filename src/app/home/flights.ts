export interface Flights {
  dateFrom: string;
  dateTo: string;
  currency: string;
  price: number;
}
