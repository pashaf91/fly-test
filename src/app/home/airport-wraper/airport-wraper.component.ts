import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FlightService } from '../flight.service';
import { NgForm } from '@angular/forms';
import { Country } from '../country';
import { Itinerary } from '../itinerary';

@Component({
  selector: 'app-airport-wraper',
  templateUrl: './airport-wraper.component.html',
  styleUrls: ['./airport-wraper.component.css']
})
export class AirportWraperComponent implements OnInit {
  @ViewChild('airports') ports: NgForm;
  @Output() sharedPorts = new EventEmitter();
  @Output() airportFrom = new EventEmitter<string>();
  @Output() airportTo = new EventEmitter<string>();
  @Input() group: NgForm;
  countries: Array<Country> = [];
  routes: Itinerary;
  dataOne: string;
  dataTwo: string;
  count;

  constructor(private flight: FlightService) {
  }

  ngOnInit() {
    this.flight.getAirPort().subscribe(res => {
      this.routes = res.routes;
      this.count = res.airports;
    });
  }

  receiveAirportOne($event) {
    this.sharedPorts.emit(this.ports);
    this.airportFrom.emit($event.iataCode);
    const code = $event.iataCode;
    const codes = this.routes[code];
    this.countries = this.count.filter(e => codes.some(c => c === e.iataCode));
  }

  receiveAirportTwo($event) {
    this.sharedPorts.emit(this.ports);
    this.airportTo.emit($event.iataCode);
  }

}
