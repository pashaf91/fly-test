import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DateWrapperComponent } from './date-wrapper.component';
import { DateSelectorModule } from '../date-selector/date-selector.module';

@NgModule({
    imports: [CommonModule, DateSelectorModule, ReactiveFormsModule, FormsModule],
    declarations: [DateWrapperComponent],
    exports: [DateWrapperComponent]
})
export class DateWrapperModule {
}
