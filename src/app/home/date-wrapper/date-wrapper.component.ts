import { Component, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import * as moment from 'moment';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-date-wrapper',
  templateUrl: './date-wrapper.component.html'
})
export class DateWrapperComponent {
  @ViewChild('dates') dates: NgForm;
  @Input() startDate: Date = null;
  @Input() endDate: Date = null;
  @Output() sharedDate = new EventEmitter();
  @Output() startDateChanged: EventEmitter<Date> = new EventEmitter();
  @Output() endDateChnaged: EventEmitter<Date> = new EventEmitter();

  constructor() {
  }

  onStartDateChange(value: Date) {
    this.startDate = value;
    this.startDateChanged.emit(value);
    this.sharedDate.emit(this.dates);
    const sourceValue = value && moment(value);
    const targetValue = this.endDate && moment(this.endDate);
    if (sourceValue && sourceValue.isValid() && targetValue < sourceValue) {
      this.endDate = sourceValue.add(2, 'd').toDate();
      this.endDateChnaged.emit(this.endDate);
    }
  }

  onEndDateChange(value: Date) {
    this.endDate = value;
    this.endDateChnaged.emit(value);
    this.sharedDate.emit(this.dates);
    const sourceValue = value && moment(value);
    const targetValue = this.startDate && moment(this.startDate);
    if (sourceValue && sourceValue.isValid() && targetValue && targetValue.isValid() && targetValue > sourceValue) {
      this.startDate = sourceValue.subtract(2, 'd').toDate();
      this.startDateChanged.emit(this.startDate);
    }
  }
}
