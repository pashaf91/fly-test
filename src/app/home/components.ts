import { NgModule } from '@angular/core/core';
import { DateSelectorModule } from './date-selector/date-selector.module';
import { DateWrapperModule } from './date-wrapper/date-wrapper.module';

@NgModule({
  exports: [DateSelectorModule, DateWrapperModule]
})
export class AppComponentsModule {
}
