import { Component, Input, forwardRef, Output, EventEmitter } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import * as moment from 'moment';

const CUSTOM_VALUE_ACCESSOR = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => DateSelectorComponent),
  multi: true
};

@Component({
  selector: 'app-date-selector',
  templateUrl: './date-selector.component.html',
  providers: [CUSTOM_VALUE_ACCESSOR]
})
export class DateSelectorComponent implements ControlValueAccessor {
  @Input()
  set date(value: Date) {
    this._date = this.convertToString(value);
  }
  _date: string;
  @Output() dateChange: EventEmitter<Date> = new EventEmitter();
  @Input() disabled: boolean;
  onTouched: Function;
  onChanged: Function;

  constructor() {
  }

  writeValue(value: Date) {
    this.date = value;
  }

  registerOnChange(fn: Function): void {
    this.onChanged = fn;
  }

  registerOnTouched(fn: Function): void {
    this.onTouched = fn;
  }

  setDisabledState(disabled: boolean): void {
    this.disabled = disabled;
  }

  onInputChanged(value: string) {
    const date = this.convertToDate(value);
    this.dateChange.emit(date);
    if (this.onChanged) {
      this.onChanged(date);
    }
  }

  onInputTouched(event: Event) {
    if (this.onTouched) {
      this.onTouched(event);
    }
  }

  convertToString(value: Date): string {
    const momentValue = moment(value);
    return momentValue.isValid() ? momentValue.format('YYYY-MM-DD') : '';
  }

  convertToDate(value: string): Date {
    const momentValue = moment(value);
    return momentValue.isValid() ? momentValue.toDate() : null;
  }
}
