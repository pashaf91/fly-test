import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import * as moment from 'moment';

@Component({
  selector: 'app-search-flight',
  templateUrl: './search-flight.component.html',
  styleUrls: ['./search-flight.component.css']
})
export class SearchFlightComponent implements OnInit {
  @ViewChild('f') signupForm: NgForm;
  childForm: boolean;
  dateForm: boolean;
  from: string;
  to: string;
  dForm: string;
  dTo: string;
  constructor() { }

  ngOnInit() {
  }

  getPorts($event) {
    this.childForm = $event.valid;
  }

  getDates($event) {
    this.dateForm = $event.valid;
  }

  getAirportOne($event) {
    this.dForm = $event;
  }

  getAirportTwo($event) {
    this.dTo = $event;
  }

  fromDate($event) {
    this.from = $event && moment($event).format('YYYY-MM-DD');
  }

  toDate($event) {
    this.to = $event && moment($event).format('YYYY-MM-DD');
  }

}
