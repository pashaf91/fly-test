import { Component, OnInit } from '@angular/core';
import { FlightService } from '../flight.service';
import { ActivatedRoute, Data } from '@angular/router';
import { Flights } from '../flights';

@Component({
  selector: 'app-search-result',
  templateUrl: './search-result.component.html',
  styleUrls: ['./search-result.component.css']
})
export class SearchResultComponent implements OnInit {
  cheapFlights: Array<Flights> = [];

  constructor(private flight: FlightService, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.params
    .subscribe((data: Data) => {
      this.flight.getFlight(data.dateFrom, data.dateTo,
        data.from, data.to)
        .subscribe(resp => {
          this.cheapFlights = resp.flights;
        });
    });
  }

}
