import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import 'rxjs/add/operator/map';

@Injectable()
export class FlightService {

  constructor(private http: Http) {
  }

  getAirPort() {
    const url = 'https://murmuring-ocean-10826.herokuapp.com/en/api/2/forms/flight-booking-selector/';
    return this.http.get(url).map(e => e.json());
  }

  getFlight(fromDate: any, toDate: any, destinationFrom: any, destinationTo: any) {
    const url = `https://murmuring-ocean-10826.herokuapp.com/en/api/2/flights/from/${destinationFrom}/to/${destinationTo}
    /${fromDate}/${toDate}/250/unique/?limit=15&offset-0`;
    return this.http.get(url).map(e => e.json());
  }
}
