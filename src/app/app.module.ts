import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { SearchFlightComponent } from './home/search-flight/search-flight.component';
import { SearchResultComponent } from './home/search-result/search-result.component';
import { AppRoutingModule } from './app-routing.module';
import { FlightService } from './home/flight.service';
import { AirportComponent } from './home/airport/airport.component';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { AirportWraperComponent } from './home/airport-wraper/airport-wraper.component';
import { DateWrapperModule } from './home/date-wrapper/date-wrapper.module';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SearchFlightComponent,
    SearchResultComponent,
    AirportComponent,
    AirportWraperComponent
  ],
  imports: [
    BrowserModule, AppRoutingModule, HttpModule, FormsModule, DateWrapperModule
  ],
  providers: [FlightService],
  bootstrap: [AppComponent]
})
export class AppModule { }
